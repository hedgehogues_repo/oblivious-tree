# Oblivious decision tree classifier

Деревья решения, представляющие собой модификацию классических деревьев. В отличие от *RT* этот вид деревьев в каждом листе строит гиперплоскость при помощи логистической регресси на полиномиальной фиче, задающей наилучшее разбиение пространства согласно индексу Джини. Такие деревья могут быть обобщены при помощи других алгоритмов, т.е. в листах дерева могут находится любые другие решатели: нейронные сети, иные регрессии, деревья, кластеризаторы и т.д. Данные деревья заточены под задачу классификации.

Для тестирования используются данные из этого датасета, в котором предсказывается наличие или отсутствие человека в помещении. Аннотация датасета: Экспериментальные данные, используемые для двоичной классификации (заполнение помещения) от температуры, влажности, света и CO2. Заземление на основе истины было получено из снимков с отметкой времени, которые принимались каждую минуту.

The decision trees are a modification of classical trees. Unlike *RT*, this kind of tree in each sheet builds a hyperplane using a logistic regression on a polynomial feature that specifies the best partition of space according to the Gini index. Such trees can be generalized using other algorithms, i.e. Any other solvers may be in the tree sheets: neural networks, other regressions, trees, clusterizers, etc. These trees are sharpened for classification.

Abstract: Experimental data used for binary classification (room occupancy) from Temperature,Humidity,Light and CO2. Ground-truth occupancy was obtained from time stamped pictures that were taken every minute.

