from sklearn.metrics import roc_auc_score
from oblivious_tree import ObliviousTree
import numpy as np
import pandas as pd


df = pd.read_csv('occupancy_data/datatraining.txt')
df.drop('date', axis=1, inplace=True)
y_train = df.Occupancy.as_matrix()
df.drop('Occupancy', axis=1, inplace=True)
X_train = df.as_matrix()

custom_tree = ObliviousTree(max_depth=2, min_leaf_sample=10)
custom_tree.fit(X_train, y_train)


df = pd.read_csv('occupancy_data/datatest2.txt')
df.drop('date', axis=1, inplace=True)
y_test = df.Occupancy.as_matrix()
df.drop('Occupancy', axis=1, inplace=True)
X_test = df.as_matrix()

answ = np.array(custom_tree.predict(X_test))
print(roc_auc_score(answ, list(y_test)))
