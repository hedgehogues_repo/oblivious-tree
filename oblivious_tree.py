from sklearn.base import BaseEstimator
from sklearn.base import ClassifierMixin
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import numpy as np


class ObliviousTree(BaseEstimator, ClassifierMixin):
    def __init__(self, threshold=0.5, max_depth=5, min_leaf_sample=300):
        BaseEstimator.__init__(self)
        ClassifierMixin.__init__(self)
        self.max_depth = max_depth
        self.min_leaf_sample = min_leaf_sample
        self.threshold = threshold
        self.root = Node(residual_depth=max_depth, min_leaf_sample=min_leaf_sample, threshold=threshold)
        
    def depth_search(self, current_node, x):
        if current_node.right_node is None or current_node.left_node is None:
            return current_node.class_label
        value = x[current_node.optimal_feature_index]
        predicted_value = current_node.optimal_clf.predict_proba([[value, value * value]])
        if predicted_value[0][1] > self.threshold:
            class_label = self.depth_search(current_node.left_node, x)
        else:
            class_label = self.depth_search(current_node.right_node, x)
        return class_label
    
    def lr_search(self, current_node, x, y, depth):
        y_left, x_left, y_right, x_right = current_node.splitter(x, y)
        current_node.class_label = np.uint8(np.mean(y) > self.threshold)
        if not (y_left is None or x_left is None or y_right is None or x_right is None):
            current_node.left_node = Node(residual_depth=self.max_depth-depth, min_leaf_sample=self.min_leaf_sample, threshold=self.threshold)
            current_node.right_node = Node(residual_depth=self.max_depth-depth, min_leaf_sample=self.min_leaf_sample, threshold=self.threshold)
            self.lr_search(current_node.left_node, x_left, y_left, depth + 1)
            self.lr_search(current_node.right_node, x_right, y_right, depth + 1)
        
    def fit(self, x, y):
        self.lr_search(self.root, x, y, 0)
        return self
        
    def predict(self, x):
        answer = []
        for row in x:
            class_label = self.depth_search(self.root, row)
            answer.append(class_label)
        return np.array(answer)
    
    def score(self, x, y, weights=None):
        print(y, self.predict(x), len(y), len(self.predict(x)))
        return accuracy_score(y, self.predict(x))
    
    def get_params(self, deep=True):
        return {
            'max_depth': self.max_depth, 
            'min_leaf_sample': self.min_leaf_sample
        }
    
    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)
        self.root = Node(residual_depth=self.max_depth, min_leaf_sample=self.min_leaf_sample, threshold=self.threshold)
        return self
        

class Node:
    def __init__(self, residual_depth, min_leaf_sample, threshold=0.5):
        self.left_node = None
        self.right_node = None
        self.optimal_clf = None
        self.optimal_djini_value = None
        self.residual_depth = residual_depth
        self.min_leaf_sample = min_leaf_sample
        self.threshold = threshold
        self.djini_values = []
        self.class_label = None
        self.optimal_feature_index = None
        self.class_0 = None
        self.class_1 = None
    
    def djini(self, split):
        if float(len(split)) == 0:
            p_plus = np.Inf
        else:
            p_plus = np.sum(split == 0) / float(len(split))
        return 2 * p_plus * (1 - p_plus)

    def delta_information(self, not_split, split1, split0):
        djini_value = self.djini(not_split) - \
                      len(split1) / float(len(not_split)) * self.djini(split1) - \
                      len(split0) / float(len(not_split)) * self.djini(split0)
        if np.isnan(djini_value) or np.isinf(djini_value):
            return 0
        else:
            return djini_value        

    def splitter(self, features, classes):
        optimal_left_set = None
        optimal_right_set = None
        self.class_0 = np.sum(np.uint8(classes == 0))
        self.class_1 = np.sum(np.uint8(classes == 1))
        if len(classes) < self.min_leaf_sample or self.residual_depth == 0 or self.class_1 == 0 or self.class_0 == 0:
            return None, None, None, None
        for index in range(0, len(features[0, :])):
            clf = LogisticRegression(class_weight='balanced')
            x = [[row, row * row] for row in features[:, index]]
            clf.fit(x, list(classes))
            class_labels = clf.predict_proba(x) > self.threshold
            left_set = np.where(class_labels[:, 1])
            right_set = np.where(class_labels[:, 0])
            djini_value = self.delta_information(classes, classes[left_set], classes[right_set])
            if self.optimal_djini_value is None or djini_value > self.optimal_djini_value:
                self.optimal_djini_value = djini_value
                self.optimal_clf = clf
                optimal_left_set = left_set
                optimal_right_set = right_set
                self.optimal_feature_index = index
            self.djini_values.append(djini_value)
        if self.optimal_djini_value > 0:
            return classes[optimal_left_set], features[optimal_left_set], \
                   classes[optimal_right_set], features[optimal_right_set]
        else:
            return None, None, None, None
