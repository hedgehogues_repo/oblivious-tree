from oblivious_tree import ObliviousTree
import pandas as pd
import numpy as np

df = pd.read_csv('occupancy_data/datatraining.txt')
df.drop('date', axis=1, inplace=True)
y_train = df.Occupancy.as_matrix()
df.drop('Occupancy', axis=1, inplace=True)
X_train = df.as_matrix()

custom_tree = ObliviousTree(max_depth=1)
custom_tree.fit(X_train, y_train)
index = 350
print(custom_tree.predict([X_train[index, :]]))

node = list(range(0, 7))
node[0] = custom_tree.root
node[1] = custom_tree.root.left_node
node[2] = custom_tree.root.right_node
node[3] = custom_tree.root.left_node.left_node
node[4] = custom_tree.root.left_node.right_node
node[5] = custom_tree.root.right_node.left_node
node[6] = custom_tree.root.right_node.right_node

print([nd.class_label for nd in node])

opt_ind = list(range(0, 7))
for i in range(0, 7):
    opt_ind[i] = node[i].optimal_feature_index

pred = node[0].optimal_clf.predict_proba([X_train[index, opt_ind[0]], X_train[index, opt_ind[0]] ** 2])[0][1]
print('0', pred, y_train[index])
if node[0].optimal_clf.predict_proba(np.array([[X_train[index, opt_ind[0]], X_train[index, opt_ind[0]] ** 2]]))[0][1] > 0.5:
    pred = node[2].optimal_clf.predict_proba([X_train[index, opt_ind[2]], X_train[index, opt_ind[2]] ** 2])[0][1]
    print('2', pred, y_train[index])
    if pred > 0.5:
        print('3', node[3].class_label, y_train[index], node[3].class_0, node[3].class_1)
    else:
        print('4', node[4].class_label, y_train[index], node[4].class_0, node[4].class_1)
else:
    pred = node[0].optimal_clf.predict_proba([X_train[index, opt_ind[1]], X_train[index, opt_ind[1]] ** 2])[0][1]
    print('1', pred, y_train[index])
    if pred > 0.5:
        print('5', node[5].class_label, y_train[index], node[5].class_0, node[5].class_1)
    else:
        print('6', node[6].class_label, y_train[index], node[6].class_0, node[6].class_1)